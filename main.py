print ("Starting")

import os
import sys

from kubernetes import client, config

from os import listdir

NAMESPACE = "default"

def updateConfigMap(aKubeClient, aConfigMapName):
    #Get the actual content of the configmap
    aConfigMap = getConfigMap(aKubeClient,aConfigMapName)
    print("Configmap retrieved on the server: ", aConfigMap)

    #Update the content
    #aNewContent={'titi':'titi'}
    aNewContent=GetFileContent()

    #Do the update
    aConfigMap.data=aNewContent

    #Save remote
    api_response = aKubeClient.replace_namespaced_config_map(aConfigMapName, NAMESPACE, aConfigMap)
    print("api_response: ",api_response)

def GetFileContent():
    aFolder="./dummyContent"
    aContent={}
    aFilesToInject = listdir(aFolder)
    print("aFilesToInject: ",aFilesToInject)
    for aOneFile in aFilesToInject:
        aFile = open(aFolder+"/"+aOneFile, 'r')
        aFileContent = aFile.read()
        aContent[aOneFile]=aFileContent
    return aContent

def getConfigMap(aKubeClient,aConfigMapName):
    name=aConfigMapName
    namespace=NAMESPACE

    api_response = aKubeClient.read_namespaced_config_map(name, namespace)
    print("api_response: ", api_response)
    return api_response

def createConfigMap(aKubeClient):
    # I created manualy with kubectl: kubectl create configmap myconfigmap --from-file=./configmapData
    # with 2 rand file in the folder configMapData
    pass

def getPod(aKubeClient):
    ret = aKubeClient.list_pod_for_all_namespaces(watch=False)
    for i in ret.items:
        print("%s\t%s\t%s" %(i.status.pod_ip, i.metadata.namespace, i.metadata.name))

def main():
    # Define the barer token we are going to use to authenticate.
    # See here to create the token:
    # https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/
    aToken = os.getenv('KUBE_TOKEN')
    if not aToken:
        print("Kubernetes token not provided.")
        sys.exit(1)

    # Create a configuration object
    aConfiguration = client.Configuration()

    # Specify the endpoint of your Kube cluster
    aConfiguration.host = "https://35.194.7.52:443"

    # Security part.
    # In this simple example we are not going to verify the SSL certificate of
    # the remote cluster (for simplicity reason)
    aConfiguration.verify_ssl = False
    # Nevertheless if you want to do it you can with these 2 parameters
    # configuration.verify_ssl=True
    # ssl_ca_cert is the filepath to the file that contains the certificate.
    # configuration.ssl_ca_cert="certificate"

    aConfiguration.api_key = {"authorization": "Bearer " + aToken}

    # Create a ApiClient with our config
    aApiClient = client.ApiClient(aConfiguration)

    # Do calls
    v1 = client.CoreV1Api(aApiClient)

    getConfigMap(v1,"myconfigmap")
    updateConfigMap(v1,"myconfigmap")
    getConfigMap(v1,"myconfigmap")



if __name__ == '__main__':
    main()

print ("Ending")