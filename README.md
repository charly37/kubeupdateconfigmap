# Script to update a Kube config map of a remote Kube cluster on GKE

Script to be run on Windows 10 with docker (the run command will mont the coding folder on windows path for EZ DEV)

To run: 
```
docker build -t devenv .
docker run -it -v C:\Code\RemoteKubeConfigmap\:/code devenv
```

# Requirement
- Docker
- Kube py API: python3.6 -m pip install kubernetes
- Remove Kube cluster on GKE: https://console.cloud.google.com/kubernetes
- Give cluster permission to default service account 

# Useful tip
Give permission to default service account on cluster (not clean but EZ) (from http://docs.heptio.com/content/tutorials/rbac.html)
```
kubectl create clusterrolebinding varMyClusterRoleBinding2 --clusterrole=cluster-admin --serviceaccount=default:default
```
and to create the original configmap
```
kubectl create configmap myconfigmap --from-file=./configmapData
```